#include <ThingerESP8266.h>
#include <Adafruit_BME280.h>
#include "MHZ19.h"                                        
#include <SoftwareSerial.h>

// WIFI
#define WLAN_SSID "WLAN_SSID"
#define WLAN_PASS "WLAN_PASS"

//Thinger.io
#define THINGERIO_USERNAME "THINGERIO_USERNAME"
#define THINGERIO_DEVICE_ID "THINGERIO_DEVICE_ID"
#define THINGERIO_DEVICE_CREDENTIAL "THINGERIO_DEVICE_CREDENTIAL"
ThingerESP8266 thing(THINGERIO_USERNAME, THINGERIO_DEVICE_ID, THINGERIO_DEVICE_CREDENTIAL);

// BME280
// NODEMCU V2 pinout
// VIN >> 3V3
// GND >> GND
// SCL >> D1
// SDA >> D2
Adafruit_BME280 bme;

// MHZ19B
// NODEMCU V2 pinout
// Vin >> Vin
// GND >> GND
// RX >> D7
// TX >> D6
#define MH_Z19_RX D7
#define MH_Z19_TX D6
MHZ19 mMHZ19;
SoftwareSerial co2Serial(MH_Z19_RX, MH_Z19_TX);

void setup() {
  Serial.begin(9600);
  Serial.println();

  //BME280
  Serial.println("BME280");
  unsigned status = bme.begin(0x76, &Wire);
  if (!status) {
    Serial.println("Could not find a valid BME280 sensor, check wiring, address, sensor ID!");
    Serial.print("SensorID was: 0x"); Serial.println(bme.sensorID(), 16);
    Serial.print("        ID of 0xFF probably means a bad address, a BMP 180 or BMP 085\n");
    Serial.print("   ID of 0x56-0x58 represents a BMP 280,\n");
    Serial.print("        ID of 0x60 represents a BME 280.\n");
    Serial.print("        ID of 0x61 represents a BME 680.\n");
    while (1) delay(1000);
  }
  else
  {
    Serial.println("BME280 Sensor detected!");
  }

  //MHZ19
  Serial.println("MHZ 19B");
  co2Serial.begin(9600);
  mMHZ19.begin(co2Serial);
  mMHZ19.autoCalibration();

  //Thinger.io
  thing.add_wifi(WLAN_SSID, WLAN_PASS);
  thing["temperature"] >> outputValue(bme.readTemperature());
  thing["pressure"] >> outputValue(bme.readPressure() / 100.0F);
  thing["co2"] >> outputValue(mMHZ19.getCO2());
  thing["temperature2"] >> outputValue(mMHZ19.getTemperature());
}

void loop() {
  //Thinger.io
  thing.handle();
}
